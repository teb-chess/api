import { Schema } from '@tsed/mongoose';
import { Enum, Property, Required } from '@tsed/common';
import Address from './Address';

@Schema()
export default class UserPersonalInformation {
    @Required()
    name: string;

    @Property()
    surname: string;

    @Property()
    middlename: string;

    @Property()
    phone: string;

    @Property()
    nationality: string;

    @Property()
    citizenship: string;

    @Enum(0, 1)
    gender: number;

    @Property()
    address: Address;
}
