import { Schema } from '@tsed/mongoose';
import { Property, Required } from '@tsed/common';

@Schema()
export default class Address {
    @Required
    country: string;

    @Property()
    state: string;

    @Property()
    city: string;

    @Property()
    address: string;

    @Property()
    zipCode: string;
}
