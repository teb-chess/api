import { GlobalAcceptMimesMiddleware, ServerLoader, ServerSettings } from '@tsed/common';
import './services/SocketService';
import credentials from './config/credentials.json';
import bodyParser from 'body-parser';
import compress from 'compression';
import cookieParser from 'cookie-parser';
import methodOverride from 'method-override';
import './middlewares/ErrorHandler';
import cors from 'cors';

const rootDir = __dirname;
console.log(process.env.NODE_ENV);

@ServerSettings({
    rootDir,
    acceptMimes: ['application/json'],
    port: process.env.port || 8080,
    statics: {
        '/': `${rootDir}/public`,
    },
    mount: {
        '/rest/v1': '${rootDir}/controllers/v1/**/*.ts',
    },
    socketIO: {
        path: '/socket',
    },
    mongoose: {
        urls: {
            default: {
                url: `${credentials.mongo}main`,
                connectionOptions: {
                    useNewUrlParser: true,
                    useUnifiedTopology: true,
                },
            },
        },
    },
    logger: {
        format: '%[%d{[yyyy-MM-dd hh:mm:ss.SSS}] %p:%] %m',
        disableRoutesSummary: true,
        logRequest: false,
        debug: false,
    },
    // debug: process.env.NODE_ENV !== 'production',
    debug: false,
})
export class Server extends ServerLoader {
    public $beforeRoutesInit(): void {
        this.use(GlobalAcceptMimesMiddleware)
            .use(cookieParser())
            .use(compress({}))
            .use(methodOverride())
            .use(bodyParser.json())
            .use(
                bodyParser.urlencoded({
                    extended: true,
                }),
            )
            .use(cors());
    }

    public $afterRoutesInit(): void {
        this.expressApp.get('/*', (req, res) => res.sendFile(`${rootDir}/public/index.html`));
    }
}
