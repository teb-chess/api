import { EndpointInfo, IMiddleware, Middleware, Req } from '@tsed/common';
import { Forbidden, Unauthorized } from 'ts-httpexceptions';
import jwt, { NotBeforeError, TokenExpiredError } from 'jsonwebtoken';
import credentials from '../config/credentials.json';

export type AuthOptions = {
    userType?: number[] | number;
    ignoreTTL: boolean;
};

@Middleware()
export class AuthMiddleware implements IMiddleware {
    private static parseHeaders(headers: any): string | null {
        return 'authorization' in headers ? headers.authorization.match(/Bearer (.*)/)[1] || null : null;
    }

    public use(@Req() request: Express.Request, @EndpointInfo() endpoint: EndpointInfo): void {
        // retrieve options given to the @UseAuth decorator
        const options: AuthOptions = endpoint.get(AuthMiddleware) || {};

        const token = AuthMiddleware.parseHeaders((request as any).headers);
        if (!token) throw new Unauthorized('provide_token');
        try {
            request.user = jwt.verify(token, credentials.jwt_secret, {
                issuer: 'schoolsystem',
                ...(options.ignoreTTL ? { ignoreExpiration: true } : {}),
            });
        } catch (e) {
            if (e instanceof TokenExpiredError) throw new Unauthorized('token_expired');
            else if (e instanceof NotBeforeError) throw new Unauthorized('token_not_active');
            else throw new Unauthorized('bad_token');
        }
        if (options.userType) {
            options.userType = Array.isArray(options.userType) ? options.userType : [options.userType];
            if (!options.userType.includes((request as any).user.typ)) throw new Forbidden('');
        }
    }
}
