import { Err, GlobalErrorHandlerMiddleware, OverrideProvider, Req, Res } from '@tsed/common';

@OverrideProvider(GlobalErrorHandlerMiddleware)
export class MyGEHMiddleware extends GlobalErrorHandlerMiddleware {
    use(@Err() error: any, @Req() request: Req, @Res() response: Res): any {
        const errorSummary: any = {};

        if (error.status) {
            errorSummary.code = error.status;
            errorSummary.status = error.name;
        } else {
            errorSummary.code = 500;
            errorSummary.status = 'INTERNAL_SERVER_ERROR';
        }

        if (error.message) errorSummary.message = error.message;

        response.status(errorSummary.code).send(JSON.stringify(errorSummary));

        return super.use(error, request, response);
    }
}
