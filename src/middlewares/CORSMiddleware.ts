import { IMiddleware, Middleware, Res } from '@tsed/common';

export type AuthOptions = {
    userType?: number[] | number;
    ignoreTTL: boolean;
};

@Middleware()
export class CORSMiddleware implements IMiddleware {
    public use(@Res() response: Res): void {
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE'); // todo fill this
        response.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization'); // todo fill this
    }
}
