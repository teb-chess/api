import { BodyParams, Controller, Get, Post, Req, UseAuth } from '@tsed/common';
import { UserService } from '../../services/UserService';
import {
    BadRequest,
    Gone,
    ImATeapot,
    InternalServerError,
    MethodNotAllowed,
    Unauthorized,
    UnavailabledForLegalReasons,
} from 'ts-httpexceptions';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import credentials from '../../config/credentials.json';
import { Util } from '../../Util';
import { JWT } from '../../types/JWT';
import User from '../../models/User';
import { AuthMiddleware } from '../../middlewares/AuthMiddleware';
import UserType from '../../types/UserType';

@Controller('/auth')
export class AuthController {
    constructor(private userService: UserService) {}

    public static restrictedLogins: (string | RegExp)[] = [/c?hu[jy]/, /pizda/, /ху[йё]/, /пизда?/];

    private static generateToken(user: User, rememberMe: boolean): string {
        return jwt.sign(
            {
                typ: 0,
                rmb: rememberMe,
                rtm: 604800,
                rid: user.refreshID,
            },
            credentials.jwt_secret,
            {
                expiresIn: '3h',
                issuer: 'chess',
                subject: user._id.toString(),
                jwtid: Util.randomString(20),
            },
        );
    }

    @Post('/')
    async authorize(
        @Req() req: Req,
        @BodyParams('login') login: string,
        @BodyParams('password') password: string,
        @BodyParams('rememberMe') rememberMe: boolean,
    ): Promise<{ token: string }> {
        const user = await this.userService.findByLogin(login);
        if (!user) throw new Unauthorized('invalid_credentials');
        const valid = await bcrypt.compare(password, user.password);
        if (!valid) throw new Unauthorized('invalid_credentials');
        // todo add token to db for teachers

        console.log(rememberMe);
        return { token: AuthController.generateToken(user, rememberMe) };
    }

    @Post('/register')
    async register(
        @Req() req: Req,
        @BodyParams('login') login: string,
        @BodyParams('email') email: string,
        @BodyParams('password') password: string,
    ) {
        if (await this.userService.checkLogin(login)) throw new BadRequest('Login is already taken');
        if (await this.userService.checkEmail(email)) throw new BadRequest('Email is already taken');
        // todo validate request params
        const user = await this.userService.add({
            login,
            email,
            password: await bcrypt.hash(password, 12),
            refreshID: Util.randomString(20),
            tokens: [],
        });

        return { ok: true };
    }

    @Post('/refresh')
    @UseAuth(AuthMiddleware, { ignoreTTL: true })
    async refreshToken(@Req() req: Req): Promise<{ token: string }> {
        const id = (req.user as JWT).sub;
        const user = await this.userService.findById(id);
        if (!user) throw new BadRequest('bad_token');
        // if ((req.user as JWT).rid !== user.refreshID) throw new BadRequest('token_revoked');
        if ((req.user as JWT).exp + (req.user as JWT).rtm < Date.now() / 1000) throw new BadRequest('token_expired');

        // todo check for token in db for teachers
        return { token: AuthController.generateToken(user, (req.user as JWT).rmb) };
    }

    @Get('/invalidate')
    @UseAuth(AuthMiddleware)
    async invalidateTokens(@Req() req: Req): Promise<{}> {
        await this.userService.changeRefreshID((req.user as JWT).sub);
        return {};
    }
}
