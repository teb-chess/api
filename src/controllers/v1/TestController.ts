import { BodyParams, Controller, Get, PathParams, Post, Req, UseAuth } from '@tsed/common';
import { APISocketService } from '../../services/SocketService';
import { AuthMiddleware } from '../../middlewares/AuthMiddleware';
import { JWT } from '../../types/JWT';

@Controller('/test')
export class TestController {
    constructor(private socketService: APISocketService) {}

    @Get()
    @UseAuth(AuthMiddleware)
    list(@Req() req: Req): any {
        return (req.user as JWT).sub;
    }

    @Post('/lol/:userID')
    broadcast(@PathParams('userID') userID: string, @BodyParams('message') message: string): any {
        for (const socket of this.socketService.getByUserID(userID)) {
            socket.emit('testEventOutput', message);
        }
    }
}
