import { Emit, Input, IO, Nsp, Socket, SocketService } from '@tsed/socketio';
import * as SocketIO from 'socket.io';
import { $log, Inject } from '@tsed/common';
import { UserService } from './UserService';
import jwt from 'jsonwebtoken';
import credentials from '../config/credentials.json';
import { JWT } from '../types/JWT';
import UserType from '../types/UserType';
import CustomSocket from '../types/CustomSocket';

@SocketService()
export class APISocketService {
    @Nsp nsp!: SocketIO.Namespace;
    @Inject(UserService)
    userService!: UserService;

    constructor(@IO private io: SocketIO.Server) {}

    getByUserID(id: string): (Socket | CustomSocket)[] {
        return Object.values(this.nsp.sockets).filter((s) => (s as CustomSocket).user.sub === id) || [];
    }

    @Input('testEventInput')
    @Emit('testEventOutput')
    lol(@Socket socket: CustomSocket): string {
        return socket.user.sub;
    }

    async $onConnection(@Socket socket: CustomSocket): Promise<void> {
        try {
            const jwtPayload = jwt.verify(socket.handshake.query.token, credentials.jwt_secret) as JWT;
            if (jwtPayload.typ === UserType.SCHOOL_WORKER) {
                if (!jwtPayload.jti) throw new Error();
                const valid = await this.userService.checkToken(jwtPayload.jti, jwtPayload.sub);
                if (!valid) throw new Error();
            }
            socket.user = jwtPayload;
            $log.debug(`Client connected: ${socket.id} ${jwtPayload.sub}`);
        } catch (e) {
            socket.error('wtf with your token, man');
            socket.disconnect();
        }
    }
}
