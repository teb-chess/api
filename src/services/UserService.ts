import { Inject, Service } from '@tsed/common';
import { MongooseModel } from '@tsed/mongoose';
import { $log } from 'ts-log-debug';
import User from '../models/User';
import { Util } from '../Util';
import { BadRequest } from 'ts-httpexceptions';

@Service()
export class UserService {
    @Inject(User)
    private User: MongooseModel<User>;

    $onInit(): void {
        this.reload().catch((e: any) => {
            throw e;
        });
    }

    async reload(): Promise<void> {
        const users = await this.User.find({});

        // todo remove this @deprecated
        if (users.length === 0) {
            $log.warn('0 users in DB');
        } else {
            $log.info(`${users.length} users in DB`);
        }
    }

    async findById(id: string): Promise<User> {
        $log.debug('Search a user from ID', id);
        const user = await this.User.findById(id).exec();
        if (!user) {
            throw new Error();
        }

        $log.debug('Found', user);
        return user;
    }

    async add(user: Omit<User, '_id'>): Promise<User> {
        $log.debug({ message: 'Validate user', user });

        const model = new this.User(user);
        $log.debug({ message: 'Save user', user });
        await model.updateOne(user, { upsert: true });

        $log.debug({ message: 'User saved', model });

        return model;
    }

    async find(options = {}): Promise<User[]> {
        return this.User.find(options).exec();
    }

    async findOne(options = {}): Promise<User[]> {
        return this.User.find(options).exec();
    }

    async findByLogin(login: string): Promise<User | null> {
        return this.User.findOne({ $or: [{ login }, { customLogin: login }, { email: login }] }).exec();
    }

    async remove(id: string): Promise<User> {
        return await this.User.findById(id).remove().exec();
    }

    async checkToken(token: string, userID: string): Promise<boolean> {
        const user = await this.User.findOne({ tokens: token });
        if (!user) return false;
        return user._id === userID;
    }

    async changeRefreshID(id: string): Promise<void> {
        const user = await this.User.findById(id).exec();
        if (!user) throw new BadRequest('unknown_id');
        user.refreshID = Util.randomString(20);
        await user.save();
    }

    async checkLogin(login: string): Promise<boolean> {
        return Boolean(await this.User.findOne({ login }));
    }

    async checkEmail(email: string): Promise<boolean> {
        return Boolean(await this.User.findOne({ email }));
    }
}
