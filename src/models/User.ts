import { Enum, Property, Required } from '@tsed/common';
import { Model, ObjectID, Unique } from '@tsed/mongoose';
import UserPersonalInformation from '../schemas/UserPersonalInformation';
import UserType from '../types/UserType';

@Model({
    collection: 'users',
})
export default class User {
    @Unique()
    @ObjectID('id')
    _id: string;

    @Required()
    login: string;

    @Property()
    email: string;

    @Property()
    tokens: string[];

    @Required()
    password: string;

    @Required()
    refreshID: string;
}
