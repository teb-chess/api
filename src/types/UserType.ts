enum UserType {
    STUDENT, // token living for 3 hours, refresh in 7 days
    PARENT, // token living for 3 hours, refresh in 7 days
    SCHOOL_WORKER, // token living for 45 minutes, refresh in a 3 days, storing and checking JWTID in db for instant token revoke
}

export default UserType;
