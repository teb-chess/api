import UserType from './UserType';

export type JWT = {
    sub: string;
    iss: 'schoolsystem';
    exp: number;
    jti: string;
    typ: UserType;
    rmb: boolean;
    rtm: number;
    rid: string;
};
