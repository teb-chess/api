import crypto from 'crypto';

export namespace Util {
    export function randomString(len: number): string {
        return crypto
            .randomBytes(Math.ceil(len / 2))
            .toString('hex')
            .slice(0, len)
            .toUpperCase();
    }
}
